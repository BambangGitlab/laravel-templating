<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/bambang', function () {
//     return view('welcome');
// });

// Route::get('/', function () {
//     return view('home');
// });

// Route::get('/register', function() {
//     return view('register');
// });

Route::get('/', 'IndexController@home');
Route::get('/register', 'AuthController@register');
Route::post('/action_page.php', 'AuthController@kirim');

Route::get('/data-table', function(){
    return view('table.data-table');
});

Route::group(['middleware' => ['web']], function () {
    // CRUD Cast
    // create
    Route::get('/cast/create', 'CastController@create');  //route menuju form create
    Route::post('/cast', 'CastController@store'); //route menyipan data ke tabel cast

    //read
    Route::get('/cast', 'CastController@index'); //route list cast
    Route::get('/cast/{cast_id}', 'CastController@show'); //route detail cast

    //update
    Route::get('/cast/{cast_id}/edit', 'CastController@edit'); //route menuju edit
    Route::put('/cast/{cast_id}', 'CastController@update'); //route untuk update

    //delete
    Route::delete('/cast/{cast_id}', 'CastController@destroy'); //route untuk hapus

    // Profile
    Route::resource('profile', 'ProfileController')->only([
        'index', 'update'
    ]);


});

// CRUD Genre
    Route::get('/genre/create', 'GenreController@create'); //Creat Menuju form genre
    Route::post('/genre', 'GenreController@store'); // Creat Menyimpan data genre
    Route::get('/genre', 'GenreController@index'); //Read list genre
    Route::get('/genre/{genre_id}', 'GenreController@show'); //Read detail genre


    //CRUD Film
    Route::resource('film', 'FilmController');

Auth::routes();



