@extends('layout.master')

@section('judul')
Halaman Index
@endsection

@section('content')
    <h1>Selamat Datang! {{$fname}} {{$lname}}</h1>
    <p>Terima kasih telah bergabung di Website kami. Media Belajar kita semua!</p>
@endsection