@extends('layout.master')

@section('judul')
Halaman Edit Profile
@endsection

@section('content')

<form action="/profile/{{$profile->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Nama User</label>
        <input type="text" value="{{$profile->user}}" class="form-control" disabled>
    </div>
    
    <div class="form-group">
        <label>Email User</label>
        <input type="text" value="{{$profile->user}}" class="form-control" disabled>
    </div>

    <div class="form-group">
        <label>Umur</label>
        <input type="number" nama='umur' value="{{$profile->umur}}" class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>bio</label>
        <textarea name= 'bio' class="form-control">{{$profile->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Alamat</label>
        <textarea name= 'alamat' class="form-control">{{$profile->alamat}}</textarea>
    </div>
    @error('alamat')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection
