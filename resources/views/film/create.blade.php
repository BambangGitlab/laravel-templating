@extends('layout.master')

@section('judul')
Tambah Film
@endsection

@section('content')

<form action="/film" method="POST">
    @csrf
    <div class="form-group">
        <label>judul</label>
        <input type="text" name='judul' class="form-control">
    </div>
    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Ringkasan</label>
        <textarea name="ringkasan" class= "form-control"></textarea>
    </div>
    @error('ringkasan')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>tahun</label>
        <input type="number" name='tahun' class="form-control">
    </div>
    @error('tahun')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Poster</label>
        <textarea name= 'bio' class="form-control"></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection