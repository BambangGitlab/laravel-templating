<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Film;

class FilmController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }

    public function create(){
        return view('film.create');
    }

    public function store(Request $request)
     {
        //Check ke Database
        // dd($request->all());
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required',
            'genre_id' => 'required',
        ]);

        $film = new film;
 
        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->tahun = $request->tahun;
        $film->poster = $request->poster;
        $film->genre_id = $request->genre_id;
 
        $film->save();

        return redirect('/film');
    }

    public function index() 
    {
        $film = Film::all();
        return view('film.index', compact('film'));
    }

    public function show($film_id)
    {
        $film = Film::where('id', $film_id)->first();
        return view('film.show', compact('film'));
    }

    public function edit($film_id)
    {
        $film = Film::where('id', $film_id)->first();
        return view('film.edit', compact('film'));
    }

    public function update(Request $request, $film_id)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required',
            'genre_id' => 'required',
        ]);

        $film = Film::find($film_id);
 
        $film->judul = $request['judul'];
        $film->ringkasan = $request['ringkasan'];
        $film->tahun = $request['tahun'];
        $film->poster = $request['poster'];
        $film->genre_id = $request['genre_id'];
 
        $film->save();

        return redirect('/film');
    }

    public function destroy($film_id)
    {
        $film = Film::find($film_id);
 
        $film->delete();

        return redirect('/film');
    }
}
