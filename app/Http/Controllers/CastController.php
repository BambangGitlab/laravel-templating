<?php

namespace App\Http\Controllers;

use App\Cast;
use Illuminate\Http\Request;

class CastController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
 
    }

    public function create(){
        return view('cast.create');
    }
    public function store(Request $request)
     {
        //Check ke Database
        // dd($request->all());
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $cast = new Cast;
 
        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;
 
        $cast->save();

        return redirect('/cast');
    }

    public function index() 
    {
        $cast = Cast::all();
        return view('cast.index', compact('cast'));
    }

    public function show($cast_id)
    {
        $cast = Cast::where('id', $cast_id)->first();
        return view('cast.show', compact('cast'));
    }

    public function edit($cast_id)
    {
        $cast = Cast::where('id', $cast_id)->first();
        return view('cast.edit', compact('cast'));
    }

    public function update(Request $request, $cast_id)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $cast = Cast::find($cast_id);
 
        $cast->nama = $request['nama'];
        $cast->umur = $request['umur'];
        $cast->bio = $request['bio'];
 
        $cast->save();

        return redirect('/cast');
    }

    public function destroy($cast_id)
    {
        $cast = Cast::find($cast_id);
 
        $cast->delete();

        return redirect('/cast');
    }
} 
