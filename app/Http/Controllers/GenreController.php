<?php

namespace App\Http\Controllers;

use App\Genre;
use Illuminate\Http\Request;

class GenreController extends Controller
{
    public function create() 
    {
        return view('genre.create');
    }
    public function store(Request $request)
    {
        // dd($request->all()); //Check ke Database
        $request->validate([
            'name'=>'required',
        ]);

        $genre = new Genre;
        $genre->name = $request->name;
        $genre->save();

       return redirect('/genre/create');
    }

    public function index() 
    {
        $genre = Genre::all();
        return view('genre.index', compact('genre'));
    }

    public function show($genre_id)
    {
        $genre = Genre::where('id', $genre_id)->first();
        return view('genre.show', compact('genre'));
    }
}
