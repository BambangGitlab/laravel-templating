<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class profile extends Model
{
    protected $table ='profile';
    protected $fillable = ['umur', 'bio', 'alamat', 'users_id'];

    public function user()
        {
            return $this->belongsTo('App\User');
        }
    
}
