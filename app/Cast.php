<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cast extends Model
{
    protected $table ='Cast';
    protected $fillable = ['nama', 'umur', 'bio'];

    public function peran()
    {
        return $this->hasMany('App\peran');
    }
}
